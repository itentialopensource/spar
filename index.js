/* eslint no-console:off */

const chalk = require('chalk');
const pkgVersion = require('./package.json').version;
const { createCI, createCLI } = require('./lib/create');

const args = process.argv.slice(2);
const command = args.shift();

const help = () => {
  const helpText = `
spar helps to create standard Kubernetes manifests enhanced with kustomize

usage:
  spar <command>

help          Print this menu
version       Output the current version of spar
create        Prompts guided CLI tool to create manifests
ci            Attempt automated manifests creation
`;
  console.log(chalk.blue(helpText));
};

module.exports = () => {
  switch (command) {
    case 'help':
      help();
      break;
    case 'create':
      createCLI();
      break;
    case 'ci':
      createCI();
      break;
    case 'version':
      console.log(chalk.green(pkgVersion));
      break;
    default:
      console.log(`\n${chalk.yellow('No command found. Please enter a command:')}`);
      help();
  }
};
