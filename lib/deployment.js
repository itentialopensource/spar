const fs = require('fs');
/**
 * createDeployment creates a deployment.yml in the manifest directory populated
 * with basic attributes of a deployment.
 * @param {string} name the name of the application being deployed
 * @param {string} port the exposed port the application runs on
 * @param {string} registry the URI where the application's container image exists
 * we default to [gitlab-reg] as the imagePullSecrets
 */
function createDeployment(name, port, registry) {
  const deployment = `apiVersion: apps/v1
kind: Deployment
metadata:
  name: ${name}
spec:
  replicas: 1
  revisionHistoryLimit: 1
  template:
    metadata:
      name: ${name}
    spec:
      containers:
        - name: ${name}
          image: ${registry}
          ports:
            - containerPort: ${port}
      imagePullSecrets:
        - name: gitlab-reg # create a secret in your cluster if required`;

  // write file to manifest
  fs.writeFile('./manifests/deployment.yml', deployment, 'utf8', (err) => {
    if (err) { throw new Error(err); }
  });
}

module.exports = createDeployment;
