const fs = require('fs');

/**
 * createService creates the service resource manifest
 * @param {string} name of the application
 * @param {string} port where the application is running
 */
function createService(name, port) {
  const service = `apiVersion: v1
kind: Service
metadata:
  name: ${name}
spec:
  ports:
  - port: 80
    targetPort: ${port}
  type: LoadBalancer`;

  // write file to manifest
  fs.writeFile('./manifests/service.yml', service, 'utf8', (err) => {
    if (err) { throw new Error(err); }
  });
}

module.exports = createService;
