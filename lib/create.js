/* eslint no-console:off */
const fs = require('fs');
const path = require('path');
const { execSync } = require('child_process');
const createDeployment = require('./deployment');
const createService = require('./service');
const createKustomize = require('./kustomize');
// manifest directory
const manifests = path.join(process.cwd(), './manifests');

/**
 * createCI assumes a [gitlab] CI enviroment and produces a manifests directory
 * based on that
 */
function createCI() {
  // check for the kustomization.yml and deployment. If both files exists, exit
  if (
    fs.existsSync(`${manifests}/kustomization.yml`)
    && fs.existsSync(`${manifests}/deployment.yml`)
  ) {
    return 'kustomize already enabled!';
  }

  // pull the CI_* env vars from Gitlab's runner environment (default to string)
  const {
    CI_PROJECT_NAME = 'replace_with_app_name',
    CI_REGISTRY_IMAGE = 'replace_with_app_registry',
    ARGO_APP_PORT, // don't default here because we can extract it
  } = process.env;

  let port;
  if (ARGO_APP_PORT) {
    port = ARGO_APP_PORT;
  } else {
    // search the local Dockerfile for the application's runtime port
    port = execSync(
      'grep -m 1 -F "EXPOSE" ./Dockerfile* | cut -d" " -f2',
    ).toString().trim();
    // if the local dockerfile doesn't expose port, set default
    if (!port) {
      port = 'replace_with_app_port';
    }
  }
  const name = CI_PROJECT_NAME.trim();
  const registry = CI_REGISTRY_IMAGE.trim();

  // create the kustomization.yml if a deployment exists
  if (
    !fs.existsSync(`${manifests}/kustomization.yml`)
    && fs.existsSync(`${manifests}/deployment.yml`)
  ) {
    createKustomize(name, registry);
    return 'kustomization.yml created';
  }

  // create the entire manifests directory if it doesn't exist
  fs.mkdirSync(manifests);
  // create manifests/deployment.yml
  createDeployment(name, port, registry);
  // create manifests/service.yml
  createService(name, port);
  // create manifests/kustomization.yml
  createKustomize(name, registry);

  return 'manifests added. kustomize enabled!';
}

/**
 * createCLI is the guided interactive CLI tool to generate manifests
 * with kustomize
 */
function createCLI() {}

module.exports = { createCI, createCLI };
