const fs = require('fs');

/**
 * createKustomization creates the kustomization.yml file used by kustomize to deploy
 * kubernetes application
 * @param {string} name of the application
 * @param {string} registry URI of application container image
 */
function createKustomization(name, registry) {
  const kustomization = `
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization

nameSuffix: ""

commonLabels:
  app: ${name}

resources:
- deployment.yml
- service.yml
# - ingress.yml
# - mcrt.yml

images:
- name: ${registry}
  newTag: ""
`;

  // write file to manifest
  fs.writeFile('./manifests/kustomization.yml', kustomization, 'utf8', (err) => {
    if (err) { throw new Error(err); }
  });
}

module.exports = createKustomization;
