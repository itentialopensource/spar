const fs = require('fs');
const { execSync } = require('child_process');
const path = require('path');
const { createCI } = require('./../../lib/create');

const manifests = path.join(process.cwd(), './manifests');

afterEach(() => {
  // remove manifests dir after each test
  execSync(`rm -rf ${manifests}`);
});

test('call createCI() without env vars set creates annotations', (done) => {
  createCI();
  expect(fs.existsSync(manifests)).toBe(true);
  fs.readFile(`${manifests}/deployment.yml`, 'utf8', (err, data) => {
    if (err) { throw new Error(err); }
    // if running in pipeline, check for project name
    if (process.env.CI_PROJECT_NAME) {
      expect(data.includes('spar')).toBe(true);
    } else {
      // else, we're running locally so use the default
      expect(data.includes('replace_with_app_name')).toBe(true);
    }
    expect(data.includes('replace_with_app_port')).toBe(true);
    // registry not set up for spar in pipeline
    expect(data.includes('replace_with_app_registry')).toBe(true);
    done();
  });
});

test('call createCI() when manifests exist', (done) => {
  createCI();
  expect(fs.existsSync(manifests)).toBe(true);
  execSync('sleep 1');
  expect(createCI()).toEqual('kustomize already enabled!');
  done();
});
