## 2.1.0 [06-07-2019]
* :heavy_minus_sign: remove manifests commit

See merge request itentialopensource/spar!5

---

## 2.0.3 [06-07-2019]
* :bug: update pipeline for deploy

See merge request itentialopensource/spar!4

---

## 2.0.2 [06-07-2019]
* :bug: update pipeline for deploy

See merge request itentialopensource/spar!4

---

## 2.0.1 [06-07-2019]
* :bug: update pipeline for deploy

See merge request itentialopensource/spar!4

---

## 2.0.0 [06-07-2019]
* :gem: update spar into a migration tool for kustomize

See merge request itentialopensource/spar!3

---

## 1.0.14 [05-17-2019]
* :heavy_plus_sign: update for new deployment format

Closes itential/cloud-team/cloudteam#92

See merge request itentialopensource/spar!1

---

## 1.0.13 [05-14-2019]
* :heavy_plus_sign: update for new deployment format

Closes itential/cloud-team/cloudteam#92

See merge request itentialopensource/spar!1

---

## 1.0.12 [05-07-2019]
* Bug fixes and performance improvements

See commit 4c26eff

---

## 1.0.11 [05-03-2019]
* Bug fixes and performance improvements

See commit 88b7764

---

## 1.0.10 [04-26-2019]
* Bug fixes and performance improvements

See commit 86a0a8a

---

## 1.0.9 [04-26-2019]
* Bug fixes and performance improvements

See commit 92d840d

---

## 1.0.8 [04-23-2019]
* Bug fixes and performance improvements

See commit be46d0a

---

## 1.0.7 [04-23-2019]
* Bug fixes and performance improvements

See commit 1699b27

---

## 1.0.6 [04-23-2019]
* Bug fixes and performance improvements

See commit e40c733

---

## 1.0.5 [04-22-2019]
* Bug fixes and performance improvements

See commit e8768a3

---

## 1.0.4 [04-22-2019]
* Bug fixes and performance improvements

See commit 3746fb6

---

## 1.0.3 [04-22-2019]
* Bug fixes and performance improvements

See commit b7c6f26

---

## 1.0.2 [04-22-2019]
* Bug fixes and performance improvements

See commit 4bd6eef

---

## 1.0.1 [04-22-2019]
* Bug fixes and performance improvements

See commit bdf2535

---
