![](https://gitlab.com/itential/cloud-team/spar/uploads/94a7193aab297e11ca94a6780a652027/image.png)

_Photo by Jason D on Unsplash_

# Spar (Iceland Spar - Sunstone)

## Overview

spar is a guided CLI an dpipeline plugin to generate kubernetes manifests utilizing kustomize.

spar has two modes:

* Automated - as a pipeline plugin, spar uses the environment and surrounding resources to populate the manifests directory when one is not present.
* Guided CLI - the guided CLI provides a set of questions that dynamically generates the manifests directory populated with the corresponding kubernetes resources in YAML.

Kubernetes' 1.14 release, introduced kustomize into kubectl as the choice for kubernetes object management. spar aims to ease the migration path to the use of kustomize by supplying what we consider to be the minimum viable YAML to get started.

### Automated

We see users coming to spar from 3 upgrade paths:

| Start                            | Upgrade Path                                | Result                                 |
| :--------------------------------| :-------------------------------------------| :------------------------------------- |
| No manifests                     | create manifests and kustomization.yml      | manifests + kustomization file         |
| Manifests without kustomize      | create kustomization.yml based on manifests | manifests + kustomization file         |
| Full implementation of kustomize | verify and suggest improvements             | suggestions for kustomize improvements |

Automated - similarly, when spar is run in an automated mode, a deployment, service, and kustomization file are created however spar takes care to look at the application's filesystem and attempt to intelligently populate the YAML such that it can be deployed by Argo and `kubernetes_deploy`

| Variables           | Example Value                                 | Description                            |
| :------------------ | :-------------------------------------------- | :------------------------------------- |
| ARGO_APP_NAME       | `hello-app`                                   | Application name seperated with hyphen |
| ARGO_APP_PORT       | `8080`                                        | Application Running Port               |
| ARGO_APP_REGISTRY   | `registry.gitlab.com/itentialopensource/spar` | Location of application image          |

### Guided CLI

In the current release the Guided CLI only handles the "First Time Create" path, and therefore will dutifully exit if a manifests directory exists.

*an available cluster and authentication are assumed by the above*

*`spar` partners best in a development pipeline with [argo](https://gitlab.com/itentialopensource/argo)*

## Installation

spar is packaged as an npm module and is available on the public npm registry at:

```bash
npm install -g @itentialopensource/spar
```

## Usage

### Automated

Integrated with [argo](https://gitlab.com/itentialopensource/argo) and enabled when `kubernetes_deploy` is used.

### Guided CLI

```bash
spar create
```

## License & Maintainers

### Maintained by

Itential Cloud Team

### License

[Apache 2.0](./LICENSE)

### Icelandic Sunstone/Spar

[An integrated] command line tool to build Kubernetes manifests

It has been speculated that the sunstone (Old Norse: sólarsteinn, a different mineral from the gem-quality sunstone) mentioned in medieval Icelandic texts was Iceland spar, and that Vikings used its light-polarizing property to tell the direction of the sun on cloudy days for navigational purposes.

[wikipedia](https://en.wikipedia.org/wiki/Iceland_spar)
